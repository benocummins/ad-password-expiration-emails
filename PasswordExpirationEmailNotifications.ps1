#Imports Active Directory module so that you can run AD PowerShell commands
import-module ActiveDirectory;

#Stores max password age in a variable
#In this instance, a fine grained password policy is being used via Password Settings Object
$maxPasswordAgeTimeSpan = (Get-ADFineGrainedPasswordPolicy 10CharacterPSO).MaxPasswordAge
 
#This section gets all of the users within AD and the necessary properties we are looking at in order to calculate password expiration dates and notify the correct email address
Get-ADUser -filter * -properties PasswordLastSet, PasswordExpired, PasswordNeverExpires, EmailAddress, GivenName | foreach {
 
    $today=get-date
    $UserName=$_.GivenName
    $Email=$_.EmailAddress
 
    if (!$_.PasswordExpired -and !$_.PasswordNeverExpires) {
 
        $ExpiryDate=$_.PasswordLastSet + $maxPasswordAgeTimeSpan
        $DaysLeft=($ExpiryDate-$today).days
 
        if ($DaysLeft -lt 14 -and $DaysLeft -gt 0){

#This section builds out the email that the end user will receive 
        $WarnMsg = "
<p style='font-family:calibri'>Hi $UserName,</p>
<p style='font-family:calibri'>Your Windows login password will expire in $DaysLeft days, please change your password before it expires by pressing CTRL-ALT-DEL and change your password.  As a reminder, you will have to enter your new password in any mobile device that you receive company email on.</p>

<p style='font-family:calibri'>Requirements for the password are as follows:</p>
<ul style='font-family:calibri'>
<li>Must be at least 10 characters long in length</li>
<li>Must not contain the user's account name or parts of the user's full name that exceed two consecutive characters</li>
<li>Must not be one of your last 7 passwords</li>
<li>Contain characters from 3 of the 4 following categories:</li>
    <ul>
    <li>English uppercase characters (A through Z)</li>
    <li>English lowercase characters (a through z)</li>
    <li>Base 10 digits (0 through 9)</li>
    <li>Non-alphabetic characters (for example, !, $, #, %)</li>
    </ul>
</ul>

<p style='font-family:calibri'>-YOUR SIGNATURE NAME HERE</p>
 
"


#This section is a loop that sends the previously created email message to each end user per their email address in AD
ForEach ($email in $_.EmailAddress) { 
send-mailmessage -to $email -from FROM@EMAIL.COM -Subject "Password Reminder: Your password will expire in $DaysLeft days" -body $WarnMsg  -smtpserver YOUR.SMTP.SERVER.HERE -BodyAsHtml }

   	}
 
    }
}

#This can be commented out if you would like, it simply sends an email notification that the script has run properly
send-mailmessage -to YOU@EMAIL.COM -from FROM@EMAIL.COM -Subject "PasswordExpirationEmailNotifications.ps1 Ran Successfully!" -smtpserver YOUR.SMTP.SERVER.HERE -BodyAsHtml
